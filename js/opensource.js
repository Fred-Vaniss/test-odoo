// Test 1 (Javascript)
function openSource() {
    output.innerHTML = `<strong>OpenSource: début du script</strong><hr>`

    for (let i = 1; i <= 99; i++) {
        if (i % 3 == 0 && i % 7 == 0) {
            log("OpenSource");
            output.innerHTML += 'OpenSource <br>'
        }
        else if (i % 3 == 0) {
            log("Open");
            output.innerHTML += 'Open <br>'
        }
        else if (i % 7 == 0) {
            log("Source");
            output.innerHTML += 'Source <br>'
        }
        else {
            log(i);
            output.innerHTML += i+'<br>'
        }
    }
    output.innerHTML += '<hr><strong>Fin du script</strong>'
}
