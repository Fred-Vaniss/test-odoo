// Test 2 (Javascript)    

let attempts
let verification
let min
let max
let maxfloor
let solution

function guess() {
    output.innerHTML = `<strong>Guess: début du script</strong><br>`
    attempts = 0
    verification = -1
    min = 1
    max = 1000000
    maxfloor = 1000000
    solution = Math.floor(Math.random() * 1000000) + 1;
    
    log(`Solution = ${solution}`)
    output.innerHTML += `La solution à trouver est ${solution}<hr>`

    // While loop (stops when attemps exceed 50x or the verification returns 0)
    while (attempts <= 50 && verification !== 0) {
        // Create a guess iterator
        let guess = Math.floor(Math.random() * maxfloor) + min;
        // Execute the verify function and return the number 0, -1 or 1
        verification = verify(guess);
        // Console log of verification
        log(`Attempt # ${attempts} = ${verification} (${guess})`);
        output.innerHTML += `Tentative # ${attempts} = ${verification} (${guess}) <br>`
        // Console log "you won" if the answer is correct
        if (verification == 0) {
            log("You won!");
            output.innerHTML += `<hr><strong>Le script à trouvé la bonne réponse!</strong>`
        } else if (attempts >= 50){
            log("You lose!")
            output.innerHTML += `<hr><strong>Le scripte n'a pas pu trouver la bonne réponse...</strong>`
        }
        attempts++;
    }
    output.innerHTML += `<hr> <strong>Fin du script</strong>`
}

function verify(guess)  {
    // If the solution is bigger, adjust the maximum value for the guess and returns 1
    if (guess > solution){
        max = guess;
        maxfloor = max-min+1
        return 1;
    // Else if the solution is smaller, adjust the minimum value for the guess and returns -1
    } else if ( guess < solution){
        min = guess
        maxfloor = max-min+1
        return -1;
    }
    // Else, returns 0 and the program won
    else{
        return 0
    }
}